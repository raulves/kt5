
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private int level;

   public Node() {
      this(null, null, null);
   }

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
      this.level = 1;
   }

   public static void checkString(String s) {
      if (s.contains(" ")) throw new RuntimeException(" ");
      else if (s.contains("((") && s.contains("))")) throw new RuntimeException("(( ja ))");
      else if (s.contains("()")) throw new RuntimeException("()");
      else if (s.contains(",,")) throw new RuntimeException(",,");
      else if (s.contains("\t")) throw new RuntimeException("\t");
      else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) throw new RuntimeException(", ja ), kuid mitte (");
   }

   public static Node parsePostfix (String s) {
      try {
         checkString(s);
      } catch (RuntimeException e) {
         throw new RuntimeException("Sõnes '" + s + "' on element '" + e.getMessage() + "' , mis ei ole lubatud.");
      }

      Stack<Node> stack = new Stack<>();
      Node node = new Node();
      String[] tokens = s.split("");

      boolean replaceRoot = false;
      for (int i = 0; i < tokens.length; i++) {
         String token = tokens[i].trim();
         switch (token) {
            case "(" :
               if (replaceRoot) throw new RuntimeException("Juur on juba olemas, viga tekkis sõnes: " + s);
               stack.push(node);
               node.firstChild = new Node();
               node = node.firstChild;
               if (tokens[i+1].trim().equals(",")) throw new RuntimeException("Puu tipu järel ei tohi olla koma, viga tekkis sõnes: " + s);
               break;
            case ")" :
               node = stack.pop();
               if (stack.size() == 0) replaceRoot = true;
               break;
            case "," :
               if (replaceRoot) throw new RuntimeException("Juur on juba olemas, viga tekkis sõnes: " + s);
               node.nextSibling = new Node();
               node = node.nextSibling;
               break;
            default :
               if (node.name == null) node.name = token;
               else node.name += token;
         }
      }
      return node;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   public String pseudoXML() {

      StringBuilder sb = new StringBuilder();
      sb.append(String.join("", Collections.nCopies(this.level == 1 ? 0 : (int)Math.pow(this.level, 2), " "))).append("<L").append(this.level).append("> ").append(this.name).append(" ");

      if (this.firstChild != null) {
         sb.append("\n");
         this.firstChild.level = this.level + 1;
         sb.append(this.firstChild.pseudoXML());
      }

      if (this.nextSibling != null) {
         if (!sb.toString().contains("\n")) {
            sb.append("</L").append(this.level).append(">").append("\n");
         } else {
            sb.append(String.join("", Collections.nCopies((int)Math.pow(this.level, 2), " "))).append("</L").append(this.level).append(">").append("\n");
         }

         this.nextSibling.level = this.level;
         sb.append(this.nextSibling.pseudoXML());
         return sb.toString();
      }


      if (sb.length() > 0) {
         if (sb.toString().contains("\n")) {
            sb.append(String.join("", Collections.nCopies(this.level == 1 ? 0 : (int)Math.pow(this.level, 2), " "))).append("</L").append(this.level).append(">").append("\n");
         } else {
            sb.append("</L").append(this.level).append(">").append("\n");
         }
      }



      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "((C)B,(E,F)D,G)A";
      //String s = "(B,C,D)A";
      //String s = "(B,C)A";
      //String s = "(((512,1)-,4)*,(-6,3)/)+";
      Node t = Node.parsePostfix (s);
      System.out.println(t.pseudoXML());
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
      //Node root = Node.parsePostfix ("A B");
      //System.out.println(root);
   }
}

